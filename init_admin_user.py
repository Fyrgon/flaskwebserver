#!/usr/bin/env python
# -*- coding: utf-8-*-
from datetime import datetime, date
from FlaskServer import db, models

now = datetime.now()
u = models.User(nick='admin', meno='Hlavny admin', heslo='nejake docela komplikovane heslo', mail='marcel.marcis@gmail.com',
       reg_time=now, birthday=now, is_admin=True, is_waiting=False,
       about_me=u"Dělám administraci, ale jenom pro moderátory. Nepište mi!")
db.session.add(u)
db.session.commit()
