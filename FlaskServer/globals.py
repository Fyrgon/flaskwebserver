# -*- coding: utf-8-*-
from hashlib import md5
from datetime import datetime, timedelta
from .models import User
import unicodedata

class ActiveUsers_t:
    active_users = {}

    def get_users(self):
        return self.active_users

    def user_active(self, user):
        if user in self.active_users:
            u = datetime.utcnow() - self.active_users[user]
            dist_min = u.days * 24 * 60 + u.seconds / 60
            if dist_min < 5: # aktivni v poslednich 5 minutach
                return True
            else:
                del self.active_users[user]
                return False
        return False

    def update_user(self, user):
        t = datetime.utcnow()
        t.replace(microsecond = 0)
        self.active_users[user] = t

    def logout(self, user):
        del self.active_users[user]

actual_active_users = ActiveUsers_t()

avatar_sizes = {
    "profile": 128,
    "post": 16,
}


def get_gravatar(mail, size):
    return 'http://www.gravatar.com/avatar/%s?d=mm&s=%d' % (md5(mail).hexdigest(), size)

def get_avatar(user, atype):
    link = user.get_avatar()
    if isinstance(link, unicode):
        link = unicodedata.normalize('NFKD', link).encode('ascii','ignore')

    if not isinstance(link, str):
        return "/static/page_media/%s/unknown.png" % atype
    if link == "gravatar":
        return get_gravatar(user.mail, avatar_sizes[atype])
    if link[0:1] is "#":
        return "/static/page_media/%s/%s" % (atype, link[1:])
    if link[0:1] is "%":
        return "/static/user_media/%s/%s" % (atype, link[1:])
    return link

def get_links(user):
    links = []
    links.append( {"href": "/", "text": "Home"})
    if user is not None and user.is_authenticated:
        links.append({"href": "/user/%s" % user.nick.capitalize(), "text": "MyProfile"})
        links.append({"href": "/logout", "text": "LogOut"})
    else:
        links.append({"href": "/login", "text": "LogIn"})
        links.append( {"href": "/register", "text": "Register"})
    links.append( {"href": "/about", "text": "About"})
    return links

