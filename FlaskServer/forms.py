# -*- coding: utf-8-*-
from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, PasswordField, DateField, TextAreaField
from wtforms.validators import DataRequired, InputRequired, Email, EqualTo, ValidationError
import globals

texts = {
   "LenValidMsg": 'Delka vstupu musi byt v rozmezi %d az %d znaku.',
   "EmailValidEq": 'Zadane emaily se neshoduji',
   "PassValidEq": 'Zadane hesla se neshoduji',
   "Required": 'Toto pole je nutne vyplnit',
}

class ValidLength(object):
    def __init__(self, min=0, max=-1, message=None):
        self.min = min
        self.max = max
        self.message = message is None and message or texts['LenValidMsg'] % (min, max)

    def __call__(self, form, field):
        l = field.data and len(field.data) or 0
        if l < self.min or (self.max != -1 and l > self.max):
            raise ValidationError(self.message)

class LoginForm(FlaskForm):
    logName = StringField('logName', validators=[DataRequired()])
    passwd = PasswordField('passwd', validators=[DataRequired()])
    remember_me = BooleanField('remember_me', default=False)

class RegisterForm(FlaskForm):
    emailAddr1 = StringField('e-mail1', validators=[InputRequired(texts['Required']), Email(), EqualTo('emailAddr2', texts['EmailValidEq'])])
    emailAddr2 = StringField('e-mail2', validators=[])
    passwd1 = PasswordField('passwd1', validators=[InputRequired(texts['Required']), EqualTo('passwd2', texts['PassValidEq'])])
    passwd2 = PasswordField('passwd2', validators=[])
    nick = StringField('nick', validators=[InputRequired(texts['Required']), ValidLength(3, 20)])
    name = StringField('name', validators=[ValidLength(3, 30)])
    bday = DateField('bday', validators=[], format='%d.%m.%Y')

class UserInfoForm(FlaskForm):
    mail = StringField('mail')
    nick = StringField('nick')
    name = StringField('name', validators=[ValidLength(3, 30)])
    bday = DateField('bday', validators=[], format='%d.%m.%Y')
    passwd = StringField('passwd', validators=[InputRequired(texts['Required'])])
    avatar = StringField('avatar')
    about_me = TextAreaField('about_me')

    is_admin = BooleanField('is_admin')
    is_judge = BooleanField('is_judge')
    is_moderator = BooleanField('is_moderator')
    is_user = BooleanField('is_user')
    is_banned = BooleanField('is_banned')
    is_waiting = BooleanField('is_waiting')

    last_seen = StringField('last_seen')
    roles = []

    def preset(self, userObj):
        self.preset_key(userObj)
        self.bday.data = userObj.birthday
        self.passwd.data = userObj.heslo
        self.name.data = userObj.meno
        self.about_me.data = userObj.about_me
        self.roles = userObj.what_am_i()
        self.is_admin.data = userObj.is_admin
        self.is_judge.data = userObj.is_judge
        self.is_moderator.data = userObj.is_moder
        self.is_user.data = userObj.is_user
        self.is_waiting.data = userObj.is_waiting
        self.is_banned.data = userObj.is_banned


    def preset_key(self, userObj):
        self.nick.data = userObj.nick.capitalize()
        self.avatar.data = globals.get_avatar(userObj, "profile")
        self.mail.data = userObj.mail
        self.last_seen.data = userObj.last_seen

# priklad rozsirenej validacie
#from app.models import User
#
#class EditForm(Form):
#    nickname = StringField('nickname', validators=[DataRequired()])
#    about_me = TextAreaField('about_me', validators=[Length(min=0, max=140)])
#
#    def __init__(self, original_nickname, *args, **kwargs):
#        Form.__init__(self, *args, **kwargs)
#        self.original_nickname = original_nickname
#
#    def validate(self):
#        if not Form.validate(self):
#            return False
#        if self.nickname.data == self.original_nickname:
#            return True
#        user = User.query.filter_by(nickname=self.nickname.data).first()
#        if user != None:
#            self.nickname.errors.append('This nickname is already in use. Please choose another one.')
#            return False
#        return True
