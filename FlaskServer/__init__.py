import sys, os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_openid import OpenID
from config import ADMINS, MAIL_SERVER, MAIL_PORT, MAIL_USERNAME, MAIL_PASSWORD

reload(sys)
sys.setdefaultencoding('utf8')

app = Flask(__name__)
app.config.from_object('config')

db = SQLAlchemy(app)

lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'

if not app.debug:
    import logging
    from logging.handlers import SMTPHandler, RotatingFileHandler

    # !!! debug akosi nefunguje - ak ho v run nastavim na true, tak aj tak je tu false ???
    #credentials = None
    #if MAIL_USERNAME or MAIL_PASSWORD:
    #    credentials = (MAIL_USERNAME, MAIL_PASSWORD)
    #else:
    #    print "App in prod mode without set credentials. See config.py"

    #msg = "Flask App Failure"
    #mail_handler = SMTPHandler((MAIL_SERVER, MAIL_PORT), 'no-reply@' + MAIL_SERVER, ADMINS, msg, credentials)
    #mail_handler.setLevel(logging.ERROR)

    file_handler = RotatingFileHandler('FlaskAppErrors.log', 'a', 1 * 1024 * 1024, 10)
    file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s;%(lineno)d]'))
    file_handler.setLevel(logging.INFO)

    #app.logger.addHandler(mail_handler)
    app.logger.addHandler(file_handler)
    app.logger.setLevel(logging.INFO)
    app.logger.info('Flask App')

from FlaskServer import views, models
