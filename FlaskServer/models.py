from FlaskServer import db
import enum


class User(db.Model):
    __tablename__ = 'User'
    nick  = db.Column(db.String(20), primary_key=True)  # identifikator
    meno  = db.Column(db.String(50))  # osloveni
    heslo = db.Column(db.String(255))  # prihlasovaci heslo
    mail  = db.Column(db.String(120), index=True, unique=True)  # kontaktni email
    reg_time  = db.Column(db.DateTime)  # cas registrace
    birthday  = db.Column(db.Date)  # narozeniny
    about_me  = db.Column(db.Text)  # osobni informace pro verejnost
    last_seen = db.Column(db.DateTime)  # posledni prihlaseni
    avatar_link = db.Column(db.Text)  # link pro avatar - obrazek
    is_admin = db.Column(db.Boolean, default=False)  # ma admin prava
    is_judge = db.Column(db.Boolean, default=False)  # ma soudcovske prava - posuzuje pribehy
    is_moder = db.Column(db.Boolean, default=False)  # je moderatorem - tvori pravidla, zodpovida za soudce
    is_user = db.Column(db.Boolean, default=False)  # je hracem
    is_banned = db.Column(db.Boolean, default=False)  # dostal BAN - nesmi nic
    is_waiting = db.Column(db.Boolean, default=True)  # ceka na schvaleni registrace, nesmi nic, nez psat do spolecneho
    mute = None

    prispevky = db.relationship('Message', backref='author', lazy='dynamic')  # vsechny prispevky uzivatele
    konverzace = db.relationship('Conversation', backref='discuter', lazy='dynamic')  # vsechny konverzace, kam muze psat

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return unicode(self.nick)  # python 2
        except NameError:
            return str(self.nick) # python 3

    def get_avatar(self):
        if self.is_waiting:
            return "/static/page_media/icon/pending.png"
        if self.is_banned:
            return "/static/page_media/icon/banned.png"
        return self.avatar_link


    def what_am_i(self):
        what_am_i = []
        if self.is_admin:
            what_am_i.append("admin")
        if self.is_judge:
            what_am_i.append("judge")
        if self.is_moder:
            what_am_i.append("moderator")
        if self.is_user:
            what_am_i.append("user")
        if self.is_banned:
            what_am_i.append("banned")
        if self.is_waiting:
            what_am_i.append("pending")
        return what_am_i

    def i_can_be(self, roles):
        if roles is None or roles == "":
            return True
        my_roles = self.what_am_i()
        for role in roles:
            if role in my_roles:
                return True
        return False

    def __repr__(self):
        rights = ""
        if self.is_admin:
            rights += "A"
        if self.is_judge:
            rights += "J"
        if self.is_moder:
            rights += "M"
        if self.is_user:
            rights += "U"
        if self.is_banned:
            rights += "B"
        if self.is_waiting:
            rights += "W"
        return '<User %r>: %r %r %r %r %r %r' % (self.nick, self.meno, self.heslo, self.mail, str(self.reg_time), str(self.birthday), rights)


class Chanel(db.Model):
    __tablename__ = 'Chanel'
    id = db.Column(db.Integer, primary_key=True)  # id kanalu
    name = db.Column(db.String(20))  # viditelne jmeno
    owner = db.Column(db.String(20), db.ForeignKey('User.nick'))  # vlastnik
    konverzace = db.relationship('Conversation', backref='chanel', lazy='dynamic')  # kto vsechno sem muze psat

    def __repr__(self):
        return "<Chanel %r>: %r %r %r %r" % (self.id, self.name, self.owner)


class Conversation(db.Model):
    __tablename__ = 'Conversation'
    chanel_id = db.Column(db.Integer, db.ForeignKey('Chanel.id'), primary_key=True)  # na kterem kanalu se muze psat
    user_id = db.Column(db.String(20), db.ForeignKey('User.nick'), primary_key=True)  # kto muze psat (s kym)


class Message(db.Model):
    __tablename__ = 'Message'
    id = db.Column(db.Integer, primary_key=True)  # id prispevku
    time = db.Column(db.DateTime)  # cas vlozeni
    text = db.Column(db.Text)  # text prispevku
    author_id = db.Column(db.String(20), db.ForeignKey('User.nick'))  # autor
    chanel_id = db.Column(db.Integer, db.ForeignKey('Chanel.id'))  # kanal kam prispevek patri

    def __repr__(self):
        return '<Post by %r, %r, %r>: %r>' % (self.author_id, str(self.time), self.chanel_id, self.text)
