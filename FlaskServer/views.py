# -*- coding: utf-8-*-
from datetime import datetime, timedelta
from flask import render_template, flash, redirect, url_for, request, g, Markup, escape, jsonify
from flask_login import login_user, logout_user, current_user, login_required, fresh_login_required
from sqlalchemy import or_, and_, desc
from urlparse import urlparse, urljoin

from FlaskServer import app, lm, db
from .forms import LoginForm, RegisterForm, UserInfoForm
from .models import User, Message

import re
import cgi
import logging
import lxml.html
from urllib2 import urlopen
from urlparse import urlparse

pages = ["http://www.google.com", "https://www.facebook.com/?ref=tn_tnmn", "http://www.washingtontimes.com/news/2010/dec/3/debt-panel-fails-test-vote/"]

import globals
from smilelist import *


def log_anomalies(anomaly):
    msg = "Anomalie - %s" % anomaly
    flash(msg)
    logging.warning("%s - %s [%s]" % (str(datetime.now()), msg, request.remote_addr))


def get_redirect(called_by, default="index"):
    next_page = request.args.get('next') or request.referrer
    if not is_safe_url(next_page):
        log_anomalies("%s: neplatny next [%s]", called_by, next_page)
        print next_page;
        return flask.abort(400)

    return redirect(next_page or url_for(default))


def user_valid(user):
    if user is None:
        return False
    if user.is_anonymous:
        return False
    if not user.is_active or not user.is_authenticated:
        return False
    if user.get_id() is None:
        return False
    return True


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and ref_url.netloc == test_url.netloc


def get_users():
    user_list = []
    if user_valid(g.user):
        if g.user.is_waiting or g.user.is_banned:
            return [[g.user.nick.capitalize(), True], ]
        users = User.query.all()
        for user in users:
            special = False
            if user.is_waiting or user.is_banned:
                if g.user.is_admin or g.user.is_moder:
                    special = True
                else:
                   continue

            active = globals.actual_active_users.user_active(user.nick)
            user_list.append([user.nick.capitalize(), active, special])
    return user_list


def prepare_refresh(avatars, post):
    # encode_links
    text = cgi.escape(post.text)
    urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', text)
    for url in urls:
        # moc dlouho to trva, stranka se vydava pomalu, mozna pro vkladani
        #t=lxml.html.parse(urlopen(url))
        #title = t.find(".//title").text

        parsed = urlparse(url)
        path = parsed[2].split("/")
        if path[-1] != "":
            title = parsed[1] + " - " + path[-1]
        else:
            title = parsed[1] + " - " + path[-2]
        text = text.replace(url, '<a class="chat_msg_link" href='+url+'>'+title+'</a>')

    # encode_smileyes
    for section in ["Smilies", "Hand Gestures"]:
        for look_up in get_look_up_smile(section):
            adr = smile_list[section][look_up[1]][0]
            title = smile_list[section][look_up[1]][3]
            short = look_up[0]
            attrs = getSmileImgAtr(16, adr, None, short)
            replace = '<span class="chat_emote"><img %s /></span>' % attrs
            text = text.replace(look_up[0], replace)

    # prepare time text
    time = post.time.replace(microsecond=0)
    now = datetime.today().date()
    if time.date() == now:
        time = time.time()

    to_me = "all" if g.user.nick != post.author_id else False
    if len(text) > 0 and text[0] == "@":
        to_me = False
        text = text[1:]
        to_some = text[0:text.find(":")].split(", ")
        for to_s in to_some:
            if to_s == g.user.nick:
                to_me = "to_me"
                break

    return {
        "id": post.id,
        "title": post.author_id,
        "text": text,
        "time": str(time),
        "avatar": avatars[post.author_id],
        "channel": post.chanel_id,
        "to_me": to_me,
    }


def getSmileImgAtr(size, adr, title, short):
    titleText = 'title="%s"' % short
    if title is not None:
        titleText = 'title="%s: %s"' % (title, short)
    text = 'src="/static/page_media/emotes/%dx%d/%s.gif" %s value="%s"' % (
           size, size, adr, titleText, short)
    return text


class Chat(object):
    can_write = False
    can_rapid = False
    can_emotics = False
    can_bonus = False
    can_edit = False
    timeout = datetime.now()

    def __init__(self, user, last_msg):
        self.smile_list = self.getSmileys("Smilies")
        self.smile_list = self.smile_list + self.getSmileys("Hand Gestures")
        if not user.is_banned:
            self.can_write = True
        if not user.is_waiting:
            self.can_rapid = True
        if user.is_admin or user.is_moder:
            self.can_edit = True

        #if right == "write":
        #    self.can_write = True
        #elif right == "write":
        #    self.can_write = True
        #elif right == "write":
        #    self.can_write = True
        #elif right == "write":
        #    self.can_write = True
        #else:
        #    raise ValueError("Unknown right for chat: %s" % right)

        if user.mute is not None:
            self.timeout = user.mute
        elif last_msg is not None:
            if self.can_rapid:
                self.timeout = last_msg.time + timedelta(seconds=5)
            else:
                self.timeout = last_msg.time + timedelta(seconds=20)
        else:
            self.timeout = datetime(1, 1, 1)

    def waitFor(self):  # vracia (pocet dni, pocet sekund) ako dlho nemoze pisat
        wait = self.timeout - datetime.now()
        return (wait.days, wait.seconds)

    def getSmileys(self, section):
        s_list = []
        for smile in smile_list[section]:
            s_list.append('<div class="chat_msg_emote"><img %s onclick="insertEmote(this);" /></div>' %
                    getSmileImgAtr(24, smile[0], smile[3], " ".join(smile[2])))
        return s_list

def get_user_avatars():
    users = {}
    for user in User.query.all():
          users[user.nick] = globals.get_avatar(user, "post")
    return users

def get_chat():
    if not user_valid(g.user):
        return None

    query = Message.query.order_by(desc(Message.time)).filter(Message.author_id == g.user.nick)
    last_msg = query.first()
    return Chat(g.user, last_msg)

def get_personal_msgs(user):
    notifs = []
    return notifs


def get_new_registrations():
    notifs = []
    users = User.query.filter(User.is_waiting==True).all()
    for user in users:
        notif_link = "<a href='%s'>%s - %s</a>" % (url_for("user", nickname=user.nick), user.nick, user.mail)
        notifs.append(Markup(notif_link))
    return notifs

def refreshChat(chat_id, last_msg, up_to):
    try:
        up_to = int(float(up_to))
        if up_to < 10 or up_to > 1000:
            up_to = 50
    except ValueError:
        up_to = 50

    query = Message.query.order_by(desc(Message.time)).filter(Message.chanel_id == chat_id)
    if last_msg == "":
        query = query.limit(up_to)
    else:
        msg_id = last_msg.split("_")[-1]
        query = query.filter(Message.id > msg_id).limit(up_to)

    avatars = get_user_avatars()
    all_msgs = query.all()
    posts = []
    for post in all_msgs:
        posts = [prepare_refresh(avatars, post)] + posts

    return {"msgs": posts, "id": chat_id}

@app.route('/request', methods=['POST'])
@login_required
def ajax_request():
    print request.form
    if request.form["req"] == "ahoj":
        return "ahoj"
    if request.form["req"] == "chat":
        out = refreshChat(request.form["id"], request.form["last"], request.form["up_to"])
        return jsonify(out);
    if request.form["req"] == "msg":
        print request.form["id"], request.form["text"]
        new_msg = Message(time=datetime.now(), text=request.form["text"], author_id=g.user.nick, chanel_id=request.form["id"])
        db.session.add(new_msg)
        db.session.commit()

    return "error";

def get_default_template_params(title=0, form=None):
    return {
        'title': title,
        'user': g.user,
        'chat': get_chat(),
        'form': form,
        'nav_links': globals.get_links(g.user),
        'user_list': get_users(),
    }

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    notifs = []
    if user_valid(g.user):
        notifs.append({"class": "personal", "notifs": get_personal_msgs(g.user), "title": "Nove osobni spravy"})

        if g.user.i_can_be(["admin", "moderator"]):
            notifs.append({"class": "registration", "notifs": get_new_registrations(), "title": "Nove registrace"})
    templ_params = get_default_template_params()
    templ_params['notifs'] = notifs
    return render_template('index.html', **templ_params)


@app.route('/about', methods=['GET', 'POST'])
def about():
    templ_params = get_default_template_params("About me")
    return render_template('about.html', **templ_params)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if user_valid(g.user):
        return get_redirect("LOGIN already logged")
    form = LoginForm()
    if not form.validate_on_submit():
        templ_params = get_default_template_params('Sign In', form)
        return render_template('login.html', **templ_params)

    name = form.logName.data.lower()
    passwd = form.passwd.data
    remember = form.remember_me.data

    user = User.query.filter(and_(User.heslo == passwd, or_(User.nick == name, User.mail == name)))
    if user.count() == 0:
        flash('Neznamy uzivatel, nebo spatne heslo')
        logging.info("Failed login name %s [%s]" % (name, request.remote_addr))
        form.logName.data = name
        templ_params = get_default_template_params('Sign In', form)
        return render_template('login.html', **templ_params)
    if user.count() > 1:
        log_anomalies("LOGIN: vic jak jeden uzivatel v DB: jmeno [%s], heslo [%s], vyseldek %s" % (name, passwd, user))

    user = user.first()

    login_user(user, remember=remember)
    logging.info("User %s logged in %s [%s]" % (user.nick, datetime.now(), request.remote_addr))

    return get_redirect("LOGIN logged OK")


@app.route('/logout')
def logout():
    if user_valid(g.user):
        flash('User %s logged out' % (g.user.meno))
        logging.info("User %s logged out [%s]" % (g.user.nick, request.remote_addr))
        globals.actual_active_users.logout(g.user.nick)
        logout_user()
    else:
        log_anomalies("LOGOUT: uzivatel neni validni [%s]" % g.user)
    return get_redirect("Logout")


@app.route('/register', methods=['GET', 'POST'])
def register():
    if user_valid(g.user):
        log_anomalies("REGISTER: aktivni uzivatel")
        return get_redirect("REGISTER already logged")
    form = RegisterForm()
    if form.validate_on_submit():
        nick = form.nick.data.lower()
        name = form.name.data
        bday = form.bday.data
        mail = form.emailAddr1.data
        passwd = form.passwd1.data

        user = User.query.filter(or_(User.mail == mail, User.nick == nick)).first()
        if user is None:
            user = User(nick=nick, meno=name, heslo=passwd, mail=mail, reg_time=datetime.now(), birthday=bday)
            db.session.add(user)
            db.session.commit()
            flash('Uzivatel %s %s byl vytvoren' % (nick, mail))
            logging.info("Registered user %s [%s]" % (user, request.remote_addr))
            login_user(user)
            return redirect(url_for("user", nickname=nick))
        else:
            logging.info("Dual registration attemt nick: %s - %s, mail: %s - %s [%s]" %
                (user.nick, nick, user.mail, mail, request.remote_addr))
            if user.nick == nick:
                flash('uzivatel %s uz existuje' % nick)
            else:
                flash('Emailova adresa %s je uz pouzivana' % mail)
    templ_params = get_default_template_params('Register', form)
    return render_template('register.html', **templ_params)

# zmena hesla
#@fresh_login_required


@app.route('/user/<nickname>', methods=['GET', 'POST'])
@login_required
def user(nickname):
    user = User.query.filter_by(nick=nickname.lower()).first()
    if user is None:
        flash('User %s not found.' % nickname)
        return get_redirect("User %s" % nickname)

    form = UserInfoForm()
    if g.user.is_admin and form.is_submitted():
        u = User.query.filter_by(nick=user.nick).first()
        u.is_admin = form.is_admin.data
        u.is_judge = form.is_judge.data
        u.is_moder = form.is_moderator.data
        u.is_user = form.is_user.data
        u.is_waiting = form.is_waiting.data
        u.is_banned = form.is_banned.data
        db.session.add(u)
        db.session.commit()
    elif form.validate_on_submit():
        if user.nick is g.user.nick:
            flash("Zmenene udaje")
            form.preset_key(g.user)
            g.user.meno = form.name.data
            g.user.heslo = form.passwd.data
            g.user.birthday = form.bday.data
            g.user.about_me = form.about_me.data
            db.session.add(g.user)
            db.session.commit()
            user = g.user
        else:
            flash("Neautorizovana změna")
    form.preset(user)

    title = "%s profile" % nickname
    owner = user.nick is g.user.nick
    admin = g.user.is_admin
    templ_params = get_default_template_params(title, form)
    templ_params['owner'] = owner
    return render_template('user.html', **templ_params)


@app.errorhandler(404)
def page_not_found(err):
    logging.info("Requesting nonexisting page %s [%s]" % (err, request.remote_addr))
    templ_params = get_default_template_params()
    return render_template('404.html', **templ_params), 404

@app.errorhandler(500)
def internal_error(err):
    db.session.rollback()
    templ_params = get_default_template_params()
    return render_template('500.html', **templ_params), 500


@app.before_request
def before_request():
    g.user = current_user
    if user_valid(g.user):
        globals.actual_active_users.update_user(g.user.nick)
        t = datetime.utcnow()
        s = datetime(t.year, t.month, t.day, t.hour, t.minute, t.second, 0)
        g.user.last_seen = s
        db.session.add(g.user)
        db.session.commit()


@lm.user_loader
def load_user(nick):
    return User.query.get(nick)
